resource "docker_network" "gitlab_network" {
  name = "gitlab-network"
}
resource "docker_container" "gitlab" {
  name  = "gitlab"
  image = "gitlab/gitlab-ee:latest"

  ports {
    internal = 22
    external = 23
  }

  ports {
    internal = 80
    external = 81
  }

  ports {
    internal = 443
    external = 444
  }

#  volumes {
#    host_path      = var.gitlab_config_path
#    container_path = "/etc/gitlab"
#  }
#
#  volumes {
#    host_path      = var.gitlab_logs_path
#    container_path = "/var/log/gitlab"
#  }
#
#  volumes {
#    host_path      = var.gitlab_data_path
#    container_path = "/var/opt/gitlab"
#  }


  network_mode = "gitlab-network"

  restart = "always"
}
