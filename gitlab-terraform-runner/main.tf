# Define your GitLab Runner Docker container
resource "docker_container" "gitlab_runner" {
  name  = "gitlab-runner"
  image = "gitlab/gitlab-runner:latest"
  # Add any other necessary configurations here
  network_mode = "gitlab-network"
}

# Wait for the container to start before registering the GitLab Runner
resource "null_resource" "wait_for_container" {
  depends_on = [docker_container.gitlab_runner]

  triggers = {
    container_id = docker_container.gitlab_runner.id
  }

  provisioner "local-exec" {
    command = <<EOF
      # Wait for the container to be fully started (you may need to adjust the sleep duration)
      sleep 10
    EOF
  }
}
# Register the GitLab Runner and capture the output
resource "null_resource" "register_runner" {
  depends_on = [null_resource.wait_for_container]

  provisioner "local-exec" {
    command = <<EOF
      output=$(docker exec gitlab-runner gitlab-runner register \
        --non-interactive \
        --url "http://gitlab" \
        --registration-token "GR1348941txQMTs6qyNXohBpF1o3B" \
        --executor "docker" \
        --docker-image "gitlab/gitlab-ee:15.9.0-ee.0" \
        --tag-list "my-gitlab-ee-runner" 2>&1)
      echo "$output" > gitlab_runner_output.txt
    EOF
  }


}

