output "gitlab_container_id" {
  description = "Container ID of the deployed GitLab instance"
  value       = docker_container.gitlab.id
}
